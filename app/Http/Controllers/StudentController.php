<?php

namespace App\Http\Controllers;
use App\Student;

use Illuminate\Http\Request;

class StudentController extends Controller
{

    public function addstudent(){
    return view('addstudent');
   
}
public function poststudent(Request $request){
    $student=Student::create($request->all());
    return redirect()->back()->with('success','student registered');

}
public function viewstudents(){
    $student=Student::all();
    
    return view('viewstudents',compact('student'));
}
public function editstudent($id){
    $student=Student::find($id);
    return view('editstudent',compact('student'));

}
public function remove($id){
    $students=Student::find($id);
    $students->delete();
    return redirect()->back()->with('success','student removed successfully');
}
public function update(Request $request,$id){
    $students=Student::find($id);
    $students->update($request->all());
    return redirect()->back()->with('success','student updated successfully');
}

}
