<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/addstudent', 'StudentController@addstudent')->name('addstudent');
Route::post('/addstudent', 'StudentController@poststudent')->name('poststudent');
Route::get('/viewstudents','StudentController@viewstudents')->name('viewstudents');
Route::get('/editstudent/student{id}','StudentController@editstudent')->name('editstudent');
Route::get('/remove/student{id}','StudentController@remove')->name('remove');
Route::post('/update/student{id}','StudentController@update')->name('update');
